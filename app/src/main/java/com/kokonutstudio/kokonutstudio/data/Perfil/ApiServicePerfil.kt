package com.kokonutstudio.kokonutstudio.data.Perfil

import com.kokonutstudio.kokonutstudio.models.DataLogin
import com.kokonutstudio.kokonutstudio.models.DatosPerfil
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface ApiServicePerfil {
    @POST("user/profile")
    fun userProfile(
        @Header("app_version") version:String?,
        @Header("lang") lang:String?,
        @Header("Accept") accept:String?,
        @Header("Authorization") authorization:String?

    ): Call<DatosPerfil>
}