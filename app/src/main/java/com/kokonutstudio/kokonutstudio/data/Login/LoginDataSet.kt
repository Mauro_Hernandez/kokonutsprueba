package com.kokonutstudio.kokonutstudio.data.Login

import android.util.Log
import com.google.gson.Gson
import com.kokonutstudio.kokonutstudio.exceptions.Exceptions
import com.kokonutstudio.kokonutstudio.models.AccessLogin
import com.kokonutstudio.kokonutstudio.models.DataLogin
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class LoginDataSet {

    val APP_VERSION="Android 1.0"
    val LANG="es_mx"
    val ACCEPT="aplication/json"
    lateinit var service: ApiServiceLogin
    var accessLogin:AccessLogin?=null

    fun setupServive(){
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://dev.estandares.kokonutstudio.com:8080/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create<ApiServiceLogin>(ApiServiceLogin::class.java)
    }

    suspend fun login(dataLogin: DataLogin?):Unit= suspendCancellableCoroutine {continuar->
        setupServive()

        service.login(APP_VERSION,LANG,ACCEPT,dataLogin).enqueue(object: Callback<AccessLogin> {

            override fun onResponse(call: Call<AccessLogin>, response: Response<AccessLogin>) {
                accessLogin=response.body()
                if(accessLogin?.success==0 || accessLogin?.success == null ){
                    continuar.resumeWithException(Exceptions(accessLogin?.message))
                }else{
                    continuar.resume(Unit)
                }

            }
            override fun onFailure(call: Call<AccessLogin>, t: Throwable) {
                Log.d("Retrofit", "Error")
                t?.printStackTrace()
                continuar.resumeWithException(Exceptions(t?.message.toString()))
            }
        })
    }

    fun getAccesLogin():AccessLogin{
        return accessLogin!!
    }
}