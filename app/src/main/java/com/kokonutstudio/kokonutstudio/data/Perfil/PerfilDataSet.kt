package com.kokonutstudio.kokonutstudio.data.Perfil

import android.util.Log
import com.kokonutstudio.kokonutstudio.exceptions.Exceptions
import com.kokonutstudio.kokonutstudio.models.AccessLogin
import com.kokonutstudio.kokonutstudio.models.DataLogin
import com.kokonutstudio.kokonutstudio.models.DatosPerfil
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class PerfilDataSet {
    val APP_VERSION="Android 1.0"
    val LANG="es_mx"
    val ACCEPT="aplication/json"
    val AUTHORIZATION="Bearer{"
    lateinit var service: ApiServicePerfil
    var datosPerfil:DatosPerfil?=null

    fun setupServive(){
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://dev.estandares.kokonutstudio.com:8080/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create<ApiServicePerfil>(ApiServicePerfil::class.java)
    }

    suspend fun userProfile(accessToken: String?):Unit= suspendCancellableCoroutine { continuar->
        setupServive()
        service.userProfile(APP_VERSION,LANG,ACCEPT,AUTHORIZATION+accessToken+"}").enqueue(object: Callback<DatosPerfil>{

            override fun onResponse(call: Call<DatosPerfil>, response: Response<DatosPerfil>) {
                datosPerfil=response.body()
                if(datosPerfil?.success==0 || datosPerfil?.success == null ){
                    continuar.resumeWithException(Exceptions(datosPerfil?.message))
                }else{
                    continuar.resume(Unit)
                }

            }
            override fun onFailure(call: Call<DatosPerfil>, t: Throwable) {
                Log.d("Retrofit", "Error")
                t?.printStackTrace()
                continuar.resumeWithException(Exceptions(t?.message.toString()))
            }
        })
    }

    fun getAllDatosPerfil():DatosPerfil{
        return datosPerfil!!
    }

}