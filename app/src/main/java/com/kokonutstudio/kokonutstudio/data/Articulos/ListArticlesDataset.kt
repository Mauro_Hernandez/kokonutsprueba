package com.kokonutstudio.kokonutstudio.data.Articulos

import android.util.Log
import com.kokonutstudio.kokonutstudio.exceptions.Exceptions
import com.kokonutstudio.kokonutstudio.models.ListArticles
import com.kokonutstudio.kokonutstudio.models.Post
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class ListArticlesDataset {
    val APP_VERSION="Android 1.0"
    val LANG="es_mx"
    val ACCEPT="aplication/json"
    lateinit var service: ApiserviceListArticles
    var articles:ListArticles?=null
    var listArt: MutableList<Post> = mutableListOf()


    fun setupServive(){
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://dev.estandares.kokonutstudio.com:8080/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create<ApiserviceListArticles>(ApiserviceListArticles::class.java)

    }

    suspend fun showPots():Unit= suspendCancellableCoroutine{continuar->
        setupServive()

            service.showPost(APP_VERSION,LANG,ACCEPT).enqueue(object: Callback<ListArticles> {
                override fun onResponse(call: Call<ListArticles>, response: Response<ListArticles>) {
                    articles=response.body()
                    setListArt(articles!!)
                    continuar.resume(Unit)
                    Log.e("Retrofit","call ${response.errorBody()}")
                    //Log.d("Retrofit", Gson().toJson(listArticles))
                }
                override fun onFailure(call: Call<ListArticles>, t: Throwable) {
                        continuar.resumeWithException(Exceptions(t.message.toString()))
                }
            })



    }


    fun setListArt(articles:ListArticles){
        listArt.clear()
        if(articles.data.listPost.size>0) {
            for (article in articles.data.listPost) {
                if (article.idPost != null)
                    listArt.add(article)
            }
        }
        //Log.d("Retrofit Lista"," ListaArticulos= ${ListArticles}")
    }

    fun getListArticles():List<Post>{
        return listArt!!
    }
}