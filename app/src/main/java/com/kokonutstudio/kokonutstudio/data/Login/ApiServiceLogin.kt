package com.kokonutstudio.kokonutstudio.data.Login

import com.kokonutstudio.kokonutstudio.models.AccessLogin
import com.kokonutstudio.kokonutstudio.models.DataLogin
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface ApiServiceLogin {
    @POST("login/")
    fun login(
        @Header("app_version") version:String?,
        @Header("lang") lang:String?,
        @Header("Accept") accept:String?,
        @Body dataLogin: DataLogin?
    ): Call<AccessLogin>
}