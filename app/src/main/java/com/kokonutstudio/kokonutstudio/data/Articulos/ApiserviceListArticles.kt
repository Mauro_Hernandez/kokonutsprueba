package com.kokonutstudio.kokonutstudio.data.Articulos

import com.kokonutstudio.kokonutstudio.models.ListArticles
import retrofit2.Call
import retrofit2.http.Header
import retrofit2.http.POST

interface ApiserviceListArticles {

    @POST("post/all")
    fun showPost(@Header("app_version") version:String?,
                    @Header("lang") lang:String?,
                    @Header("Accept") accept:String?
                    ):Call<ListArticles>

}