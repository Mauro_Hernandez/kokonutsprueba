package com.kokonutstudio.kokonutstudio.viewModel.Login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kokonutstudio.kokonutstudio.domain.Login.LoginUseCase

class LoginViewModelFactory(val loginUseCase: LoginUseCase):ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(LoginUseCase::class.java).newInstance(loginUseCase)
    }
}