package com.kokonutstudio.kokonutstudio.viewModel.Articulos

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kokonutstudio.kokonutstudio.domain.Articulos.ListArticlesUseCase
import com.kokonutstudio.kokonutstudio.exceptions.Exceptions
import com.kokonutstudio.kokonutstudio.models.Post
import com.kokonutstudio.kokonutstudio.ui.Articulos.ListArticlesFragment
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class ListArticlesViewModel(val listArticlesUseCase: ListArticlesUseCase):ViewModel(), CoroutineScope {
    private var view:ListArticlesFragment?=null
    private val listArticles= MutableLiveData<List<Post>>()
    val job=Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main+job


    fun getListaArticles(){
        launch{
            try{
                view?.showProgressBar()
                listArticlesUseCase.showPots()
                setListArticles(listArticlesUseCase.getListArticles())
                view?.hideProgressBar()
            }catch (e: Exceptions){
                view?.showException(e.message.toString())
                view?.hideProgressBar()
            }

        }

    }
    /*

    fun productDetail(article:Post){
        launch{
            try{
                view?.showProgressBar()
                listArticlesUseCase.showPots()
                setListArticles(listArticlesUseCase.getListArticles())
                view?.hideProgressBar()
            }catch (e: Exceptions){
                view?.showException(e.message.toString())
                view?.hideProgressBar()
            }

        }
    }
    */


    fun setListArticles(listArticles:List<Post>){
        this.listArticles.value=listArticles
    }


    fun getListArticlesLiveData(): LiveData<List<Post>> {
        return listArticles
    }

    fun attachView(view:ListArticlesFragment){
        this.view=view
    }
    fun dettachView(){
        this.view=null
    }

    fun dettachJob(){
        coroutineContext.cancel()
    }
}