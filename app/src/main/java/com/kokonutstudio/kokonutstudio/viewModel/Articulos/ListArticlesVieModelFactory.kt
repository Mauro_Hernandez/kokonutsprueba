package com.kokonutstudio.kokonutstudio.viewModel.Articulos

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kokonutstudio.kokonutstudio.domain.Articulos.ListArticlesUseCase

class ListArticlesVieModelFactory(val listArticlesUsecase:ListArticlesUseCase):ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(ListArticlesUseCase::class.java).newInstance(listArticlesUsecase)
    }
}