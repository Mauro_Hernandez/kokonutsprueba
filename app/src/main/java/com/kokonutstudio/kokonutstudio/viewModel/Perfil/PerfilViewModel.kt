package com.kokonutstudio.kokonutstudio.viewModel.Perfil

import androidx.lifecycle.ViewModel
import com.kokonutstudio.kokonutstudio.domain.Perfil.PerfilUseCase
import com.kokonutstudio.kokonutstudio.ui.Perfil.PerfilFragment
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class PerfilViewModel(val perfilUseCase: PerfilUseCase?):ViewModel(), CoroutineScope {
    var view: PerfilFragment?=null
    val job= Job()


    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main+job

    fun getPerfilVM(accessToken:String){
        launch {
            try{
                view?.showProgressBar()
                perfilUseCase?.userProfile(accessToken)
                if(perfilUseCase?.getAllDatosPerfil()!= null || perfilUseCase?.getAllDatosPerfil()?.data!=null){
                    val perfil=perfilUseCase?.getAllDatosPerfil()?.data
                    view?.setupViewsPerfil(perfil.name,perfil.emal,perfil.lastName,perfil.secondLastName,perfil.image)
                }
                view?.hideProgressBar()
            }catch (e:Exception){
                view?.hideProgressBar()
                view?.showException(e.message.toString())
            }
        }

    }




    fun attachView(view: PerfilFragment){
        this.view=view
    }
    fun dettachView(){
        this.view=null
    }

    fun deattachJob(){
        coroutineContext.cancel()
    }

}