package com.kokonutstudio.kokonutstudio.viewModel.Login

import android.util.Log
import androidx.lifecycle.ViewModel
import com.kokonutstudio.kokonutstudio.domain.Login.LoginUseCase
import com.kokonutstudio.kokonutstudio.ui.Login.LoginFragment
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class LoginViewModel(var loginUseCase: LoginUseCase): ViewModel(), CoroutineScope {
    var view: LoginFragment?=null
    val job= Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main+job

    fun loginVM(email:String,password:String){
        launch {
            try{
                view?.showProgressBar()
                loginUseCase?.login(email,password)
                if(loginUseCase?.getAccessLogin()!= null || loginUseCase?.getAccessLogin().data?.accessToken!=null || loginUseCase?.getAccessLogin().data?.accessToken!=""){
                    view?.setupSession(loginUseCase?.getAccessLogin())
                }
                view?.hideProgressBar()
                view?.goHome()
            }catch (e:Exception){
                view?.hideProgressBar()
                view?.showException(e.message.toString())
            }
        }

    }



    fun attachView(view:LoginFragment){
        this.view=view
    }
    fun dettachView(){
        this.view=null
    }

    fun deattachJob(){
        coroutineContext.cancel()
    }
}