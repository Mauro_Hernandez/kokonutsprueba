package com.kokonutstudio.kokonutstudio.viewModel.Perfil

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kokonutstudio.kokonutstudio.domain.Perfil.PerfilUseCase

class PerfilViewModelFactory(val perfilUseCase: PerfilUseCase): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(PerfilUseCase::class.java).newInstance(perfilUseCase)
    }
}