package com.kokonutstudio.kokonutstudio

import android.content.Context
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import android.view.MenuItem
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main_kokonuts.*

class MainKokonutsActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    val MYPREFERENCES="MYPREF"
    val ACCESS_TOKEN="ACCESS_TOKEN"

    private lateinit var appBarConfiguration: AppBarConfiguration
    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_kokonuts)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        navController= findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_perfil
            ),drawerLayout
        )

        nav_view.setupWithNavController(navController)

        nav_view.setNavigationItemSelectedListener(this)

    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        //menuInflater.inflate(R.menu.main_kokonuts, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
           // R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        return super.onSupportNavigateUp()
        val navController=findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration)||super.onSupportNavigateUp()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_perfil -> {
                findNavController(R.id.nav_host_fragment).navigate(item.itemId)
                drawer_layout.closeDrawers()
            }
            R.id.nav_signoff -> {
                val sharedPreferences = getSharedPreferences(MYPREFERENCES, Context.MODE_PRIVATE)
                if (sharedPreferences.getString(ACCESS_TOKEN,"") != "") {
                    val editor = sharedPreferences.edit()
                    editor.putString(ACCESS_TOKEN, "")
                    editor.apply()

                    //  Log.d("Login1","name= ${token}")
                }else{
                    Toast.makeText(this,"No hay una sesion iniciada", Toast.LENGTH_SHORT).show()
                }
                findNavController(R.id.nav_host_fragment).navigate(R.id.loginFragment)
                true
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }
}
