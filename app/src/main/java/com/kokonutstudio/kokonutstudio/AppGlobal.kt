package com.kokonutstudio.kokonutstudio

import android.app.Application
import android.content.Context
import com.kokonutstudio.kokonutstudio.utils.DatabaseHelper

class AppGlobal():Application() {
    companion object {
        lateinit var CONTEXT:Context
        lateinit var db:DatabaseHelper
        val DB_NAME="kokonutstudio.db"
        val VERSIONDB=1
        val TABLEARTICLES="articles"

    }

    override fun onCreate() {
        super.onCreate()
        CONTEXT=applicationContext
        db=DatabaseHelper()
    }
}