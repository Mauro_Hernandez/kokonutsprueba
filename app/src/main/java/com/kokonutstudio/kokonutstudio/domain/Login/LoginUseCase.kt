package com.kokonutstudio.kokonutstudio.domain.Login

import com.kokonutstudio.kokonutstudio.data.Login.LoginDataSet
import com.kokonutstudio.kokonutstudio.models.AccessLogin
import com.kokonutstudio.kokonutstudio.models.DataLogin

class LoginUseCase {
    private val loginDatatSet=LoginDataSet()

    suspend fun login(email:String,password:String){
        val dataLogin= DataLogin(email,password)
        loginDatatSet.login(dataLogin)
    }

    fun getAccessLogin():AccessLogin{
        return loginDatatSet.getAccesLogin()
    }
}