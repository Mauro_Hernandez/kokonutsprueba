package com.kokonutstudio.kokonutstudio.domain.Perfil

import com.kokonutstudio.kokonutstudio.data.Perfil.PerfilDataSet
import com.kokonutstudio.kokonutstudio.models.DatosPerfil

class PerfilUseCase {
    val perfilDataSet=PerfilDataSet()
    suspend fun userProfile(access_Token:String){
        perfilDataSet.userProfile(access_Token)
    }

    fun getAllDatosPerfil():DatosPerfil{
        return perfilDataSet.getAllDatosPerfil()
    }

}