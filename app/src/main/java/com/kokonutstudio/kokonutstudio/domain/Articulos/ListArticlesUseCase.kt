package com.kokonutstudio.kokonutstudio.domain.Articulos

import com.kokonutstudio.kokonutstudio.data.Articulos.ListArticlesDataset
import com.kokonutstudio.kokonutstudio.models.Post

class ListArticlesUseCase {
    private val articlesDataset=ListArticlesDataset()

    suspend fun showPots(){
       articlesDataset.showPots()
    }

    fun getListArticles():List<Post>{
        return articlesDataset.getListArticles()
    }

}