package com.kokonutstudio.kokonutstudio.ui.Articulos


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation

import com.kokonutstudio.kokonutstudio.R
import com.kokonutstudio.kokonutstudio.models.Post
import com.kokonutstudio.kokonutstudio.utils.ControllerArticle
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_article.*

class ArticleFragment : Fragment() {
    lateinit var article: Post
    lateinit var navController: NavController
    val controllerArticle= ControllerArticle()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_article, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController= Navigation.findNavController(view)
        article=ArticleFragmentArgs.fromBundle(arguments!!).Article!!
        if(article!=null){
            setupViewsArticle(article.header,article.title.toString(),article.body)
        }
        setupButtons()
    }

    fun setupViewsArticle(img:String,title:String,body:String){
        if(  img!=null || img!=""){
            Picasso.get().load(img).into(img_foto_Article)
        }
        tv_title_Article.text=title
        tv_description_Article.text=title

    }
    fun setupButtons(){
        fab_like_Article.setOnClickListener {
            controllerArticle.addArticle(article)
        }
        btn_back_Article.setOnClickListener {
            activity!!.onBackPressed()
        }
    }




}
