package com.kokonutstudio.kokonutstudio.ui.Login


import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation

import com.kokonutstudio.kokonutstudio.R
import com.kokonutstudio.kokonutstudio.domain.Login.LoginUseCase
import com.kokonutstudio.kokonutstudio.models.AccessLogin
import com.kokonutstudio.kokonutstudio.utils.CustomProgressBar
import com.kokonutstudio.kokonutstudio.viewModel.Login.LoginViewModel
import com.kokonutstudio.kokonutstudio.viewModel.Login.LoginViewModelFactory
import kotlinx.android.synthetic.main.activity_main_kokonuts.*
import kotlinx.android.synthetic.main.fragment_login.*
import java.util.regex.Pattern


class LoginFragment : Fragment() {

    lateinit var navController: NavController
    val progressBar = CustomProgressBar()
    lateinit var viewModel: LoginViewModel
    val MYPREFERENCES="MYPREF"
    val ACCESS_TOKEN="ACCESS_TOKEN"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view=inflater.inflate(R.layout.fragment_login, container, false)
        // Inflate the layout for this fragment
        setupViewModel()
        return view!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        navController= Navigation.findNavController(view)
        setupButtons()
        //Si hay aces_token cambia a buscar los articulos
        val sharedPreferences = activity!!.getSharedPreferences(MYPREFERENCES, Context.MODE_PRIVATE)
        if (sharedPreferences.getString(ACCESS_TOKEN,"") != "") {
            goHome()
        }
    }

    fun setupViewModel(){
        viewModel= ViewModelProviders.of(this, LoginViewModelFactory(LoginUseCase())).get(LoginViewModel::class.java)
        viewModel.attachView(this)
    }

    override fun onDetach() {
        super.onDetach()
        viewModel?.dettachView()
        viewModel?.deattachJob()
    }

    fun setupButtons(){
        btn_login_Login.setOnClickListener {
            //var user=GetLogin("administrador@admin.com","adminadmin")
            // api?.getUserToken(user)
            val email=et_email_Login.text.toString().trim()
            val password=et_password_Login.text.toString().trim()
            if(!email.isEmpty() && !password.isEmpty()){
                if(isValidEmail(email)) {
                    viewModel?.loginVM(email, password)
                    //goHome()
                }else{
                    Toast.makeText(activity,"El email no es valido", Toast.LENGTH_SHORT).show()
                }
            }
            else{
                Toast.makeText(activity,"Por favor, ingresa todos los datos", Toast.LENGTH_SHORT).show()
            }
        }
    }
    fun setupSession(accessLogin: AccessLogin){
        val sharedPreferences = activity!!.getSharedPreferences(MYPREFERENCES, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(ACCESS_TOKEN, accessLogin?.data?.accessToken)
        editor.apply()
        goHome()
    }

    fun isValidEmail(email: String): Boolean {
        val emailPattern = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@" + "[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$"
        val EMAIL_PATTERN = Pattern.compile(emailPattern)
        if (email != "") {
            val matcher = EMAIL_PATTERN.matcher(email)
            if (matcher.matches()) {
                return true
            } else {

            }
        }
        return false
    }

    fun goHome(){
        navController.navigate(R.id.next_List_Articles)
    }

    fun showProgressBar(){
        progressBar.show(activity!!,"Espera por favor...")
    }
    fun hideProgressBar(){
        progressBar.dialog.dismiss()
    }
    fun showException(message:String){
        //Log.d("Error","Error ${message}")
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()

    }




}
