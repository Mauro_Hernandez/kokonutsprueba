package com.kokonutstudio.kokonutstudio.ui.Perfil


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders

import com.kokonutstudio.kokonutstudio.R
import com.kokonutstudio.kokonutstudio.domain.Perfil.PerfilUseCase
import com.kokonutstudio.kokonutstudio.utils.CustomProgressBar
import com.kokonutstudio.kokonutstudio.viewModel.Perfil.PerfilViewModel
import com.kokonutstudio.kokonutstudio.viewModel.Perfil.PerfilViewModelFactory
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_perfil.*


class PerfilFragment : Fragment() {
    val MYPREFERENCES="MYPREF"
    val ACCESS_TOKEN="ACCESS_TOKEN"
    val progressBar = CustomProgressBar()
    lateinit var viewModel: PerfilViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v=inflater.inflate(R.layout.fragment_perfil, container, false)
        setupPerfilFragentViewModel()
        return v!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sharedPreferences = activity!!.getSharedPreferences(MYPREFERENCES, Context.MODE_PRIVATE)
        if (sharedPreferences.getString(ACCESS_TOKEN,"") != "") {
            viewModel.getPerfilVM(sharedPreferences.getString(ACCESS_TOKEN,""))
        }
        setupButtons()
    }

    fun setupPerfilFragentViewModel(){
        viewModel= ViewModelProviders.of(this, PerfilViewModelFactory(PerfilUseCase())).get(PerfilViewModel::class.java)
        viewModel.attachView(this)

    }

    fun setupViewsPerfil(name:String,email:String, last_name:String, second_Last_name:String, img:String){
        tv_nombre_Perfil.text=name
        tv_email_Perfil.text=email
        tv_AP_Perfil.text=last_name
        tv_AM_Perfil.text=second_Last_name
        if(!img.equals(""))
            Picasso.get().load("img").into(img_foto_Perfil)
    }
    fun setupButtons(){

        btn_back_Perfil.setOnClickListener {
            activity!!.onBackPressed()
        }
    }



    fun showProgressBar(){
        progressBar.show(activity!!,"Espera por favor...")
    }
    fun hideProgressBar(){
        progressBar.dialog.dismiss()
    }
    fun showException(message:String){
        //Log.d("Error","Error ${message}")
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()

    }
    override fun onDetach() {
        super.onDetach()
        viewModel?.dettachView()
        viewModel?.deattachJob()
    }



}
