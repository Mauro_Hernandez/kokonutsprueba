package com.kokonutstudio.kokonutstudio.ui.Articulos


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.kokonutstudio.kokonutstudio.R
import com.kokonutstudio.kokonutstudio.domain.Articulos.ListArticlesUseCase
import com.kokonutstudio.kokonutstudio.models.Post
import com.kokonutstudio.kokonutstudio.ui.Articulos.AdapterListArticles.AdapterCardviewListArticles
import com.kokonutstudio.kokonutstudio.utils.CustomProgressBar
import com.kokonutstudio.kokonutstudio.viewModel.Articulos.ListArticlesVieModelFactory
import com.kokonutstudio.kokonutstudio.viewModel.Articulos.ListArticlesViewModel
import kotlinx.android.synthetic.main.activity_main_kokonuts.*

class ListArticlesFragment : Fragment() {
    private lateinit var viewModel: ListArticlesViewModel
    private var recyclerView: RecyclerView?=null
    lateinit var navController: NavController
    val progressBar = CustomProgressBar()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view=inflater.inflate(R.layout.fragment_list_articles, container, false)
        recyclerView=view.findViewById(R.id.rv_articles_listArticles)
        setupInicioFragentViewModel()
        setObservableViewModel()
        return view!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        navController= Navigation.findNavController(view)
    }

    fun setupInicioFragentViewModel(){
        Log.d("rutas","viewModel")
        viewModel= ViewModelProviders.of(this, ListArticlesVieModelFactory(ListArticlesUseCase())).get(ListArticlesViewModel::class.java)
        viewModel.attachView(this)
        viewModel.getListaArticles()
    }

    fun setObservableViewModel(){
        Log.d("rutas","observer")
        val articlesObserver = Observer<List<Post>>{
            Log.d("articlos","articulos ="+it)
            updateRecyclerView(it)
        }
        viewModel.getListArticlesLiveData().observe(this,articlesObserver)
    }

    fun updateRecyclerView(list: List<Post>){
        var adapter= AdapterCardviewListArticles(list,this)
        recyclerView?.layoutManager= LinearLayoutManager(activity)
        recyclerView?.adapter=adapter
    }

    fun showProgressBar(){
        progressBar.show(activity!!,"Espera por favor...")
    }
    fun hideProgressBar(){
        progressBar.dialog.dismiss()
    }

    override fun onDetach() {
        super.onDetach()
        viewModel.dettachJob()
        viewModel.dettachView()
    }

    fun showException(message:String){
        //Log.d("Error","Error ${message}")
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()

    }

    fun goAllArticle(article:Post){
        val article=ListArticlesFragmentDirections.actionListArticlesFragmentToArticleFragment(article!!)
        navController.navigate(article!!)
    }




}
