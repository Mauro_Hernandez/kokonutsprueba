package com.kokonutstudio.kokonutstudio.ui.Articulos.AdapterListArticles

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kokonutstudio.kokonutstudio.R
import com.kokonutstudio.kokonutstudio.models.Post
import com.kokonutstudio.kokonutstudio.ui.Articulos.ListArticlesFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.cardview_article.view.*

class AdapterCardviewListArticles(listArticles:List<Post>,view:ListArticlesFragment): RecyclerView.Adapter<AdapterCardviewListArticles.AdapterViewHolder>() {
    var view:ListArticlesFragment?=view
    var listArticles:List<Post>?=listArticles
    var viewHolder:AdapterViewHolder?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        val v=LayoutInflater.from(parent?.context).inflate(R.layout.cardview_article,parent,false)
        viewHolder= AdapterViewHolder(v)
        return viewHolder!!

    }

    override fun getItemCount(): Int {
        return listArticles?.size!!
    }

    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {
        Picasso.get().load("${listArticles?.get(position)?.header}").into(holder.img)
        holder.description.text=listArticles?.get(position)?.title.toString()
        holder.img.setOnClickListener {
            view?.goAllArticle(listArticles?.get(position)!!)
        }
        holder.description.setOnClickListener {
            view?.goAllArticle(listArticles?.get(position)!!)
        }

    }

    class AdapterViewHolder(view:View):RecyclerView.ViewHolder(view!!){
        val img=view.img_Article_Card
        val description=view.tv_Description_Card
    }
}