package com.kokonutstudio.kokonutstudio.utils

import android.database.DatabaseUtils
import com.kokonutstudio.kokonutstudio.AppGlobal
import com.kokonutstudio.kokonutstudio.models.Post

class ControllerArticle {


    fun getAllArticle(article: Post):List<String>?{
        var listIdPost:MutableList<String> = mutableListOf()
        try{
            val db=AppGlobal.db.readableDatabase

            val numArticles=DatabaseUtils.queryNumEntries(db,AppGlobal.TABLEARTICLES).toInt()
            if(numArticles>0){
                val qryArticles= "SELECT ${Contract.Article.idPost} FROM ${AppGlobal.TABLEARTICLES};"
                val c =db.rawQuery(qryArticles,null)
                c.moveToFirst()
                do{
                   listIdPost.add(c.getString(c.getColumnIndex("${Contract.Article.idPost}")))
                }while(c.moveToNext())
                c.close()
                db.close()

            }else{

            }

        }catch (e:Exception){

        }
        return listIdPost!!

    }

    fun addArticle(article: Post){
        try{
            val db=AppGlobal.db.writableDatabase
            val qryCreateArticle= "INSERT INTO ${AppGlobal.TABLEARTICLES} ("+
                    "${Contract.Article.idPost}, ${Contract.Article.body})"+
                    " VALUES('${article.idPost}','${article.body}');"
            db.execSQL(qryCreateArticle)
        }catch (e:Exception){

        }

    }

}