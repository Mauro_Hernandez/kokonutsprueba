package com.kokonutstudio.kokonutstudio.utils

import android.app.Application
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.kokonutstudio.kokonutstudio.AppGlobal

class DatabaseHelper:SQLiteOpenHelper(AppGlobal.CONTEXT,AppGlobal.DB_NAME,null,AppGlobal.VERSIONDB) {

    val qryCreateTable="CREATE TABLE ${AppGlobal.TABLEARTICLES}("+
            "${Contract.Article.id} INTEGER PRYMERY KEY AUTOINCREMENT,"+
            "${Contract.Article.idPost} INTEGER,"+
            "${Contract.Article.body} TEXT);"
    override fun onCreate(db: SQLiteDatabase?) {
       db!!.execSQL(qryCreateTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}