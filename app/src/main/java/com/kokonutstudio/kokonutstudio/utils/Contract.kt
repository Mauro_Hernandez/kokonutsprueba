package com.kokonutstudio.kokonutstudio.utils

import android.provider.BaseColumns

class Contract {
    class Article:BaseColumns{
        companion object {
            val id="id"
            val idPost="idPost"
            val body="body"
        }
    }
}