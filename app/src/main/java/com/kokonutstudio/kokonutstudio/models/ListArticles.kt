package com.kokonutstudio.kokonutstudio.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ListArticles(
    var success: Int=0,
    var message: String="",
    var data: Data=Data()
):Parcelable