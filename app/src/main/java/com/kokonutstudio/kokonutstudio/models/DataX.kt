package com.kokonutstudio.kokonutstudio.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataX(
    @SerializedName("id_user")
    var idUser: String="",
    var name: String="",
    @SerializedName("last_name")
    var lastName: String="",
    @SerializedName("second_last_name")
    var secondLastName: String="",
    var emal: String="",
    var image: String=""
): Parcelable