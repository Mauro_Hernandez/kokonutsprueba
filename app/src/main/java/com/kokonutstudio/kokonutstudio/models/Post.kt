package com.kokonutstudio.kokonutstudio.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Post(
    var idPost: Int=0,
    var title: Int=0,
    var header: String="",
    var body: String="",
    var footer: String="",
    var slug: String="",
    @SerializedName("created_at")
    var createdAt: String="",
    @SerializedName("updated_at")
    var updatedAt: String=""
): Parcelable