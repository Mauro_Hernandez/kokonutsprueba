package com.kokonutstudio.kokonutstudio.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class AccessLogin(
    var success: Int=-1,
    var message: String="",
    var data: DataXX=DataXX()
):Parcelable