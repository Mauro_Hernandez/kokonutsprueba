package com.kokonutstudio.kokonutstudio.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataLogin(
    var username: String="",
    var password: String=""
):Parcelable