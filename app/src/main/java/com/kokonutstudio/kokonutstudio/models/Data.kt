package com.kokonutstudio.kokonutstudio.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Data(
    @SerializedName("current_page")
    var currentPage: Int=-1,
    @SerializedName("list_post")
    var listPost: List<Post> = listOf(),
    @SerializedName("first_page_url")
    var firstPageUrl: String="",
    var from: Int=-1,
    @SerializedName("last_page")
    var lastPage: String="",
    @SerializedName("clast_page_url")
    var clastPageUrl: String="",
    @SerializedName("next_page_url")
    var nextPageUrl: String="",
    var path: String="",
    @SerializedName("per_page")
    var perPage: Int=-1,
    @SerializedName("prev_page_url")
    var prevPageUrl: String="",
    var to: Int=-1,
    var total: Int=-1
): Parcelable