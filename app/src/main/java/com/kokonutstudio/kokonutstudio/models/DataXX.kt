package com.kokonutstudio.kokonutstudio.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataXX(
    @SerializedName("token_type")
    var tokenType: String="",
    @SerializedName("expires_in")
    var expiresIn: Int=-1,
    @SerializedName("access_token")
    var accessToken: String="",
    @SerializedName("refresh_token")
    var refreshToken: String=""
):Parcelable