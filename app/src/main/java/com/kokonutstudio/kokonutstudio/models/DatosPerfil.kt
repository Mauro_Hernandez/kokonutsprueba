package com.kokonutstudio.kokonutstudio.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DatosPerfil(
    var success: Int,
    var message: String,
    var data: DataX,
    var token: String=""
): Parcelable